---
layout: page
title: About
permalink: /about/
---

My name is Clifford. I've been with my lovely wife for over a decade and reside in the great flat lands of Indiana with our three children. Not only are there kids, there are cats. 3 freaking cats.

While not Batman, my day job keeps me sharp hacking on Linux which happens to be my favorite thing in the world concerning computers. I enjoy some programming (java and intepretted languages), hardware hacking, and VR related stuff.

I'd like to think of myself as a creative type, but I mostly just make a mess of paper with watercolor. I also dabble in photography with a Pentax DSLR. I really like finding old lenses to shoot on since they all work with the k mount.

Outside of all of that, I'm pretty boring really. 
