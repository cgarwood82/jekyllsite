---
layout: page 
title: And All Hell Broke Loose...
categories: blog
---

First entry into the blog here. Nothing exciting to really report on outside of finally kicking this thing off. I'm hoping to make a blog to share life experiences, technology jargon, exciting projects, photography, and anything else that seems fitting for sharing.
