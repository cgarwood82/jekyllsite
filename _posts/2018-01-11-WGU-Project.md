---
layout: page
title: WGU Project
categories: blog 
---

So this project of mine has sat idle for sometime now. My last blog post was some time over the summer of 2017. Fast forward to January 2018 and here I am again. Much larger motivation though.

My capstone project for my Bachelor of Science in Information Technology at Western Governors University.

![WGU logo](https://www.wgu.edu/sites/wgu.edu/files/custom_uploads/WGU-MarketingLogo_Natl_RGB_Owl_NoTag_4-1-2017.jpg)

The core of my project is to automate the provisioning of a cloud hosted server, configuration of the applicable services, and finally deploying this blog. I'm writing this post locally and not publishing the site just yet. 

That's part of the grand final when I conclude my project :) 

For the curious, here is some of the stuff I plan on leveraging:

* [Chef](https://www.chef.io/) for the automation component.
* [Jekyll](https://jekyllrb.com/), a static page generator for the blog
* [Bitbucket](https://bitbucket.org/) is version control for both my project and this blog
* [Vagrant](https://www.vagrantup.com/) and [VirtualBox](https://www.virtualbox.org/wiki/Downloads) for my local testing and QA
* [CentOS 7](https://www.centos.org/) is my target operating system for all testing and public server needs
